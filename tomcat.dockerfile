FROM busybox AS build-stage
COPY java/WeLoveMountains.war /WeLoveMountains/
RUN unzip /WeLoveMountains/WeLoveMountains.war -d /WeLoveMountains/
RUN rm -f /WeLoveMountains/WeLoveMountains.war
COPY config.properties /WeLoveMountains/WEB-INF/classes/config.properties

FROM tomcat:8.5.55-jdk8-openjdk-slim
COPY --from=build-stage /WeLoveMountains/ /usr/local/tomcat/webapps/WeLoveMountains/
