FROM busybox AS build-stage
COPY aspnet.core/ /WeLoveMountains/
COPY appsettings.json /WeLoveMountains/appsettings.json

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
COPY --from=build-stage /WeLoveMountains/ /apps/WeLoveMountains/
WORKDIR /apps/WeLoveMountains/
EXPOSE 80
CMD ["./welovemountains.net"]

